#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <sqlite3.h>

using std::cout;
using std::endl;

// Share one database for all tests.
sqlite3 *db{nullptr};

// Use a listener to setup and tear down the database. (based on Catch2's example)
struct MyListener : Catch::TestEventListenerBase {

  using TestEventListenerBase::TestEventListenerBase; // inherit constructor

  // Get rid of Wweak-tables
  ~MyListener();

    // The whole test run starting
    void testRunStarting(Catch::TestRunInfo const &testRunInfo) override {
        cout << "Event: testRunStarting" << endl;

        if (db == nullptr) {
            cout << "setting up db" << endl;

            sqlite3_stmt *query{nullptr};
            int rc;

            REQUIRE(sqlite3_open(":memory:", &db) == SQLITE_OK);

            std::ifstream ifs("schema.sql");
            std::string sql;
            sql.assign((std::istreambuf_iterator<char>(ifs)),
                       (std::istreambuf_iterator<char>()));

            rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &query, nullptr);
            REQUIRE(rc == SQLITE_OK);

            rc = sqlite3_step(query);
            REQUIRE(rc == SQLITE_DONE);

	    REQUIRE(SQLITE_OK == sqlite3_finalize(query));

            cout << "db setup OK" << endl;
        } else {
            cout << "db is not nullptr" << endl;
        }
    }

    // The whole test run ending
    void testRunEnded(Catch::TestRunStats const &testRunStats) override {
        cout << "closing db" << endl;
        int rc{0};
        rc = sqlite3_close(db);
        // cout << "rc = " << rc << endl;
        cout << "Event: testRunEnded" << endl;

        REQUIRE(SQLITE_OK == rc);
    }
};

CATCH_REGISTER_LISTENER(MyListener)

// Get rid of Wweak-tables
MyListener::~MyListener() {}


namespace MyTests {
    void testInsertAndRollback(const char *sectionTitle) {
        // --- unit test part 1: setup world ----------------------------------------------------------
        int rc;
        int count;

        sqlite3_stmt *query{nullptr};

        rc = sqlite3_prepare_v2(db, "begin;", -1, &query, nullptr);
        REQUIRE(SQLITE_OK == rc);
        rc = sqlite3_step(query);
        REQUIRE(SQLITE_DONE == rc);
	REQUIRE(SQLITE_OK == sqlite3_finalize(query));

        // --- unit test part 2: verify state before action -------------------------------------------
        rc = sqlite3_prepare_v2(db, "select count(*) from something;", -1, &query, nullptr);
        REQUIRE(SQLITE_OK == rc);
        rc = sqlite3_step(query);
        REQUIRE(SQLITE_ROW == rc);
        count = sqlite3_column_int(query, 0);
        cout << sectionTitle << " -- row count before action: " << count << endl;
        REQUIRE(count == 0);
	REQUIRE(SQLITE_OK == sqlite3_finalize(query));

        // --- unit test part 3: do action ------------------------------------------------------------
        rc = sqlite3_prepare_v2(db, "insert into something (name) values ('boo');", -1, &query, nullptr);
        REQUIRE(SQLITE_OK == rc);
        rc = sqlite3_step(query);
        REQUIRE(SQLITE_DONE == rc);
	REQUIRE(SQLITE_OK == sqlite3_finalize(query));

        // --- unit test part 4: verify state after action --------------------------------------------
        rc = sqlite3_prepare_v2(db, "select count(*) from something;", -1, &query, nullptr);
        REQUIRE(SQLITE_OK == rc);
        rc = sqlite3_step(query);
        REQUIRE(SQLITE_ROW == rc);
        count = sqlite3_column_int(query, 0);
        cout << sectionTitle << " -- row count after action: " << count << endl;
        REQUIRE(count == 1);
	REQUIRE(SQLITE_OK == sqlite3_finalize(query));

        // --- unit test part 5: cleanup so next test not affected ------------------------------------
        rc = sqlite3_prepare_v2(db, "rollback;", -1, &query, nullptr);
        REQUIRE(SQLITE_OK == rc);
        rc = sqlite3_step(query);
        REQUIRE(SQLITE_DONE == rc);
	REQUIRE(SQLITE_OK == sqlite3_finalize(query));
    }

    TEST_CASE("SQLite Transaction and Rollback") {
        SECTION("Add something 1") {
            testInsertAndRollback("Add something 1");
        }
        SECTION("Add something 2") {
            testInsertAndRollback("Add something 2");
        }
    }
}

