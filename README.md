# Example for using SQLite3 rollbacks in Catch2 unit tests

This example uses [Catch2](https://github.com/catchorg/Catch2) to test inserting a row
into an in-memory SQLite database.
It's meant to demonstrate best practices that result in fast unit tests that do not
affect one another. 

# Build and run the example

SQLite needs to be [installed](http://www.sqlitetutorial.net/download-install-sqlite).  
Once SQLite is installed you should be able to build and run the tests from the 
command line.

In [fish](https://fishshell.com), cd to the project's directory and ...
~~~bash
mkdir build; and cp schema.sql build/; and cd build; and cmake -H.. -B.; and make; and ./myTest
~~~

In bash, cd to the project's directory and ...
~~~bash
mkdir build && cp schema.sql build/ && cd build && cmake -H.. -B. && make && ./myTest
~~~

You can run `ctest` instead of `./myTest`, but I prefer Catch2's output from `./myTest`.

# Thoughts

* Real-life tests don't *need* to have so many Catch2 `REQUIRE` statements. This example
  uses them to be very explicit about what the return values should be from SQLite
  calls. 
* This example uses a Catch2 Listener to set up the database before any test runs and 
  close it after all tests have completed. It's overkill for this simple example but
  in large projects we only want to load the schema once for speed.
* This example loads the database schema from a file. It's so simple it could have been 
  done in-line but larger projects would probably want the separate file.
* Catch2 is included in this project as a single header file. This is simple but makes
  repeated compiles slow. The simplest fix would be to install Catch2 on your system
  and set up the CMake file to use a [separate](https://github.com/catchorg/Catch2/blob/master/docs/slow-compiles.md) 
  main.
